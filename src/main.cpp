/**
 * Sonoff RGB Blink
 *
 * Turns on an LED on for one second,
 * then off for one second, repeatedly.
 */
#include "Arduino.h"

#ifndef LED_BUILTIN
#define LED_BUILTIN 13
#endif
#define LED 13

#define SONOFF_L1_MODE_COLORFUL           1  // [Color key] Colorful (static color)
#define SONOFF_L1_MODE_COLORFUL_GRADIENT  2  // [SMOOTH] Colorful Gradient
#define SONOFF_L1_MODE_COLORFUL_BREATH    3  // [FADE] Colorful Breath
#define SONOFF_L1_MODE_DIY_GRADIENT       4  // DIY Gradient (fade in and out) [Speed 1- 100, color]
#define SONOFF_L1_MODE_DIY_PULSE          5  // DIY Pulse  (faster fade in and out) [Speed 1- 100, color]
#define SONOFF_L1_MODE_DIY_BREATH         6  // DIY Breath (toggle on/off) [Speed 1- 100, color]
#define SONOFF_L1_MODE_DIY_STROBE         7  // DIY Strobe (faster toggle on/off) [Speed 1- 100, color]
#define SONOFF_L1_MODE_RGB_GRADIENT       8  // RGB Gradient
#define SONOFF_L1_MODE_RGB_PULSE          9  // [STROBE] RGB Pulse
#define SONOFF_L1_MODE_RGB_BREATH        10  // RGB Breath
#define SONOFF_L1_MODE_RGB_STROBE        11  // [FLASH] RGB strobe
#define SONOFF_L1_MODE_SYNC_TO_MUSIC     12  // Sync to music [Speed 1- 100, sensitivity 1 - 10]


void SnfL1Send(const char *buffer) {
  Serial.print(buffer);
  Serial.write(0x1B);
  Serial.flush();
}

void SnfL1SerialSendOk(void) {
  char buffer[16];
  snprintf_P(buffer, sizeof(buffer), PSTR("AT+SEND=ok"));
  SnfL1Send(buffer);
}

void setup()
{
  // initialize LED digital pin as an output.
  pinMode(LED, OUTPUT);

  // open connection to Nuovoton
  Serial.begin(19200, SERIAL_8N1);
  Serial1.begin(19200, SERIAL_8N1);
  delay(200);

}

int incomingByte = 0;

void loop() {
  // turn lights off
  SnfL1Send("AT+UPDATE=\"sequence\":\"1\",\"mode\":12,\"sensitive\":40,\"switch\":\"on\"");
  delay(20);
  while (Serial.available() > 0) Serial.read();
  delay(20);
  SnfL1SerialSendOk();

  delay(2000);

  // read console input
  /*
  while (Serial.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial.read();
    if (incomingByte == 10 || incomingByte == 13)
      Serial1.print(0x1B);
    else
      Serial1.print(incomingByte, DEC);
    
    Serial.print((char) incomingByte);
  }
  while (Serial1.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial1.read();
    if (incomingByte == 0x1B)
      Serial.print(0x0A);
    else
      Serial.print((char) incomingByte);
  }
  */
}
