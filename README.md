# BlinkenFan

## General Hardware description

The is called **Smart Wifi Controller For LED Strip <span style="color: #F00;">R</span> <span style="color: #0F0;">G</span> <span style="color: #00F;">B</span>**. It can be bougt from [Aliexpress](https://www.aliexpress.com/item/4000036208215.html). The product name appears to be `Sonoff L1`, but on the controller the name is `Spider Z`. 

The controller board consists of an `ESP8265` for WIFI and general control. `RX/TX` and `3V3` are conveniently accessible through labelled test ports on the back side. The ESP8265 is serially connected to an `N76E003AT20` which is responsible for driving the LEDs PWM dignas. The `N76E003AT20` also has a microphone connected for added MmmmZmmmZmmmZm blinky-blink mode.

```
+---------------+              +-------------+  /- R --> +----------
| ESP 8265, 4Mb | -- UART0 --> | N76E003AT20 +-+-- G --> +  LED
| WIFI,   26MHz |              +-+--------+--+  \- B --> +  Strip
+---------------+                |        |          --> +----------
                                 |     +--+--+      /
                             +---+---+ | MIC |     /
                             | IR RX | +--+--+    /
                             +-------+           /
                                                /
+------------+                                 /
| 24v -> 12V |---------------------------------
+------------+
```

## Goals

- one pin for PWM to ESCs
- power supply 12V DC to ESC (?A?)
- power supply 24V DC to LED controller (?A?)
- use mic for strobo mode
- control via 
  - IR
  - WIFI

## Hardware 

### Sonoff L1 (DLX-WIFI DT, 20181008)

- [Tasmota Discussion](https://github.com/arendst/Tasmota/issues/4935), [Home-Assistant.io](https://community.home-assistant.io/t/cheap-amazon-led-strip/89892/7)
- [nuvoTon N76E003AT20](doc/N76E003.pdf) 932GDFA
- [ESP8285 242019](doc/0a-esp8285_datasheet_en.pdf) UBOOPNRU26
- Original Firmware (4M Rom): [hex](doc/flash_4M.hex), [bin](doc/flash_4M.bin)
- UART Connection: 
	- ESP8685 (U0TXD) `26  ---> p3` (RXD) N76E003
	- ESP8685 (U0RXD) `25 <---  p2` (TXD) N76E003
- MIC: ` 4 --> p2.0` (RST/P2.0) N76E003
- IR:  `10 --> p1.5` (PWM5/IC7/SS/P1.5) N76E003

#### Protocol description between Nuvotron <-> esp8265

Comm is at `19200/8N1`. All transmissions both direction are terminated with `ESC` (ascii: `27`, hex: `0x1B`, bin: `11011`).

[AT Command example](https://github.com/arendst/Tasmota/issues/4935)

the `sequence` seems to be unimportant, in the examples a unix timestamp is used, however `1` seems to work as well.

- [Home Assistant Forum](https://community.home-assistant.io/t/cheap-amazon-led-strip/89892)
- [Tasmota Implementation](https://github.com/arendst/Tasmota/blob/development/tasmota/xlgt_05_sonoff_l1.ino)
- 

##### On/off

```json
AT+UPDATE="sequence":"1554665120087","switch":"on"
```

##### Set color:

```json
AT+UPDATE="sequence":"1554661797561","mode":1,"colorR":74,"colorB":0,"colorG":255,"light_type":1
```

##### Set brightness:

```json
AT+UPDATE="sequence":"1554662013447","mode":1,"bright":100
```

##### Set mode:

```json
AT+UPDATE="sequence":"1554662461694","mode":1,"switch":"on"
```

##### Modes:

```
1  Colorful (static color)
2  Colorful Gradient
3  Colorful Breath
4  DIY Gradient (fade in and out) [Speed 1- 100, color]
5  DIY Pulse  (faster fade in and out) [Speed 1- 100, color]
6  DIY Breath (toggle on/off) [Speed 1- 100, color]
7  DIY Strobe (faster toggle on/off) [Speed 1- 100, color]
8  RGB Gradient
9  RGB Pulse
10 RGB Breath
11 RGB strobe
12 Sync to music (Reacts to on board mic)  [Speed 1- 100, sensitivity 1 - 10]
```

##### Set color (in mode 5 in this example)

```json
AT+UPDATE="sequence":"1554663306706","mode":5,"colorR":2,"colorG":0,"colorB":255,"light_type":1,"switch":"on"
```

##### Set speed (in mode 5 in this example) values 1 - 100

```json
AT+UPDATE="sequence":"1554663227094","speed":32,"mode":5
```

##### Set sensitivity in mode 12 (sound reactive mode), values 1 - 10

```json
AT+UPDATE="sequence":"1554663400526","mode":12,"sensitive":5
```

```C
#define SONOFF_L1_MODE_COLORFUL           1  // [Color key] Colorful (static color)
#define SONOFF_L1_MODE_COLORFUL_GRADIENT  2  // [SMOOTH] Colorful Gradient
#define SONOFF_L1_MODE_COLORFUL_BREATH    3  // [FADE] Colorful Breath
#define SONOFF_L1_MODE_DIY_GRADIENT       4  // DIY Gradient (fade in and out) [Speed 1- 100, color]
#define SONOFF_L1_MODE_DIY_PULSE          5  // DIY Pulse  (faster fade in and out) [Speed 1- 100, color]
#define SONOFF_L1_MODE_DIY_BREATH         6  // DIY Breath (toggle on/off) [Speed 1- 100, color]
#define SONOFF_L1_MODE_DIY_STROBE         7  // DIY Strobe (faster toggle on/off) [Speed 1- 100, color]
#define SONOFF_L1_MODE_RGB_GRADIENT       8  // RGB Gradient
#define SONOFF_L1_MODE_RGB_PULSE          9  // [STROBE] RGB Pulse
#define SONOFF_L1_MODE_RGB_BREATH        10  // RGB Breath
#define SONOFF_L1_MODE_RGB_STROBE        11  // [FLASH] RGB strobe
#define SONOFF_L1_MODE_SYNC_TO_MUSIC     12  // Sync to music [Speed 1- 100, sensitivity 1 - 10]


void SnfL1Send(const char *buffer) {
  Serial.print(buffer);
  Serial.write(0x1B);
  Serial.flush();
}

void SnfL1SerialSendOk(void) {
  char buffer[16];
  snprintf_P(buffer, sizeof(buffer), PSTR("AT+SEND=ok"));
  SnfL1Send(buffer);
}

void setup()
{
  // initialize LED digital pin as an output.
  pinMode(LED, OUTPUT);

  // open connection to Nuovoton
  Serial.begin(19200, SERIAL_8N1);
  Serial1.begin(19200, SERIAL_8N1);
  delay(200);

}
```

##### Example tasmota configuration without MQTT feedback

```yaml
- platform: mqtt
  name: 'RGB strip'
  command_topic: 'cmnd/rgbstrip/serialsend'
  payload_on: 'AT+UPDATE="sequence":"1","switch":"on"'
  payload_off: 'AT+UPDATE="sequence":"1","switch":"off"'
  rgb: true
  rgb_command_topic: 'cmnd/rgbstrip/serialsend'
  rgb_command_template: 'AT+UPDATE="sequence":"1","mode":1,"colorR":{{red}},"colorG":{{green}},"colorB":{{blue}},"light_type":1,"switch":"on"'
  optimistic: true
```


#### MIC Connection

![Microphone Schematic](doc/mic.jpg)

#### N76E003AT20 pin layout

![N76E003AT20 pinout](doc/N76E003AT20-pinout.png)

#### ESP8285 pin layout

![ESP8285 pinout](doc/esp8285-pinout.png)

### FTDI cable pin out

**CAVE:** must use 3V3 cable, 5V is no good for ESP8265.

![FTDI cable pin out](doc/ftdi-cable-pinout.jpg)

#### dumping original firmware

	$ ~~sudo apt get install esptool~~ # don't, it's too old
	$ sudo pip install esptool # should be >= 2.7
	$ sudo dmesg | grep ttyUSB
	[225355.371170] usb 1-5.1.1: FTDI USB Serial Device converter now attached to ttyUSB0

Check connection:

	$ esptool.py --port /dev/ttyUSB0 read_mac
	esptool.py v2.8
	Serial port /dev/ttyUSB0
	Connecting....
	Detecting chip type... ESP8266
	Chip is ESP8285
	Features: WiFi, Embedded Flash
	Crystal is 26MHz
	MAC: 2c:f4:32:c1:ab:4b
	Uploading stub...
	Running stub...
	Stub running...
	MAC: 2c:f4:32:c1:ab:4b
	Hard resetting via RTS pin...


Doanload image:
	
	$ esptool.py --port /dev/ttyUSB0 read_flash 0x00000 0x400000 flash_4M.bin
	esptool.py v2.8
	Serial port /dev/ttyUSB0
	Connecting....
	Detecting chip type... ESP8266
	Chip is ESP8285
	Features: WiFi, Embedded Flash
	Crystal is 26MHz
	MAC: 2c:f4:32:c1:ab:4b
	Uploading stub...
	Running stub...
	Stub running...
	4194304 (100 %)
	4194304 (100 %)
	Read 4194304 bytes at 0x0 in 395.1 seconds (84.9 kbit/s)...
	Hard resetting via RTS pin...

	$ xxd flash_4M.bin > flash_4M.hex

Et voila!

#### Sonoff L1

![L1 Top](doc/IMG_20200822_093625.jpg)
![L1 Bottom](doc/IMG_20200822_093641.jpg)
